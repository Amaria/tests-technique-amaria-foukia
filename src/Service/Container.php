<?php
/**
 * Created by PhpStorm.
 * User: amaria
 * Date: 21/06/18
 * Time: 16:13
 */

namespace App\Service;


class Container
{

    public function getMessage()
    {
        $messages = [
            'You did it! You updated the system! Amazing!',
            'That was one of the coolest updates I\'ve seen all day!',
            'Great work! Keep going!',
        ];

        $index = array_rand($messages);

        return $messages[$index];
    }


}
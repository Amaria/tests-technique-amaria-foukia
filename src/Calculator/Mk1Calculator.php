<?php
/**
 * Created by PhpStorm.
 * User: amaria
 * Date: 20/06/18
 * Time: 14:27
 */

namespace App\Calculator;

use App\Model\Change;

class Mk1Calculator implements CalculatorInterface
{
    /**
     * @return string Indicates the model of automaton
     */
    public function getSupportedModel(): string
    {
        return "mk1";

    }

    /**
     * @param int $amount The amount of money to turn into change
     * @return Change|null The change, or null if the operation is impossible
     */
    public function getChange(int $amount): ?Change
    {

        if(is_int($amount)){
            $change = new Change();
            $change->coin1 = $amount;
            return $change;

        } else {
            return null;
        }

    }
}
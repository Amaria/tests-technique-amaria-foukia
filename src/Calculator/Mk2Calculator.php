<?php
/**
 * Created by PhpStorm.
 * User: amaria
 * Date: 20/06/18
 * Time: 14:27
 */

namespace App\Calculator;
use App\Model\Change;

class Mk2Calculator implements CalculatorInterface
{

    /**
     * @return string Indicates the model of automaton
     */
    public function getSupportedModel(): string
    {
        // TODO: Implement getSupportedModel() method.
         return "mk2";
    }

    /**
     * @param int $amount The amount of money to turn into change
     * @return Change|null The change, or null if the operation is impossible
     */
    public function getChange(int $amount): ?Change
    {
        // TODO: Implement getChange() method.
        $change = new Change();

        while($amount>=10){
            $change->bill10++;
            $amount-=10;
        }

        while($amount>=5){
            $change->bill5++;
            $amount-=5;
        }

        while($amount>=2){
            $change->coin2++;
            $amount-=2;
        }

        if ($amount!= 0){
            return null;
        }
        else{
            return $change;
        }
    }
}
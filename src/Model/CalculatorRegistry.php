<?php
/**
 * Created by PhpStorm.
 * User: amaria
 * Date: 20/06/18
 * Time: 14:52
 */

namespace App\Model;

use App\Calculator\CalculatorInterface;
use App\Registry\CalculatorRegistryInterface ;


class CalculatorRegistry implements CalculatorRegistryInterface
{

    /**
     * @param string $model Indicates the model of automaton
     * @return CalculatorInterface|null The calculator, or null if no CalculatorInterface supports that model
     */
    public function getCalculatorFor(string $model): ?CalculatorInterface
    {
        // TODO: Implement getCalculatorFor() method.
    }
}